using System;

namespace PainfulSmile.StronglyTypedStrings
{
    /// <summary>
    /// Attach this attribute in a class that inherits from <see cref="GeneratorUnit"/> to expand the Project Settings window.
    /// </summary>
    public sealed class GeneratorExtensionAttribute : Attribute { }
}
