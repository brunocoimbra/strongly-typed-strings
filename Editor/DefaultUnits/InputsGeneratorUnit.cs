using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PainfulSmile.StronglyTypedStrings.DefaultUnits
{
    /// <summary>
    /// Generates strongly-typed strings for the Unity inputs.
    /// </summary>
    [GeneratorExtension]
    public sealed class InputsGeneratorUnit : GeneratorUnit
    {
        /// <summary>
        /// The label to be displayed on the Project Settings menu.
        /// </summary>
        public override GUIContent Label { get; } = new GUIContent("Inputs");

        /// <summary>
        /// Customize the order in the Project Settings menu.
        /// </summary>
        protected override int Priority { get; } = 10;
        /// <summary>
        /// Define what your generated type represent.
        /// </summary>
        protected override string Type { get; } = "Input";

        /// <summary>
        /// Implement your generator logic here.
        /// </summary>
        protected override void OnGenerate()
        {
            var namesSet = new HashSet<string>();
            var serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
            SerializedProperty axesProperty = serializedObject.FindProperty("m_Axes");

            foreach (SerializedProperty property in axesProperty)
            {
                string input = property.FindPropertyRelative("m_Name").stringValue;
                string name = GeneratorUtility.GetValidMemberName(input);

                if (namesSet.Contains(name))
                {
                    continue;
                }

                namesSet.Add(name);
                Append(name, input);
            }
        }
    }
}
