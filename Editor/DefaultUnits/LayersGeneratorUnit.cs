using UnityEditorInternal;
using UnityEngine;

namespace PainfulSmile.StronglyTypedStrings.DefaultUnits
{
    /// <summary>
    /// Generates strongly-typed strings for the Unity layers. It also generates strongly-typed ints for them.
    /// </summary>
    [GeneratorExtension]
    public sealed class LayersGeneratorUnit : GeneratorUnit
    {
        /// <summary>
        /// The label to be displayed on the Project Settings menu.
        /// </summary>
        public override GUIContent Label { get; } = new GUIContent("Layers");

        /// <summary>
        /// Customize the order in the Project Settings menu.
        /// </summary>
        protected override int Priority { get; } = 10;
        /// <summary>
        /// Define what your generated type represent.
        /// </summary>
        protected override string Type { get; } = "Layer";

        /// <summary>
        /// Implement your generator logic here.
        /// </summary>
        protected override void OnGenerate()
        {
            foreach (string layer in InternalEditorUtility.layers)
            {
                Append(layer, LayerMask.NameToLayer(layer));
            }
        }
    }
}
