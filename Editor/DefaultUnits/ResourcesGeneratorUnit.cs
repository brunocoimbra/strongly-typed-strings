using UnityEditor.SettingsManagement;
using UnityEngine;

namespace PainfulSmile.StronglyTypedStrings.DefaultUnits
{
    /// <summary>
    /// Generates strongly-typed paths for the contents of all special "Resources" folders in the project.
    /// </summary>
    [GeneratorExtension]
    public sealed class ResourcesGeneratorUnit : SpecialFolderContentGeneratorUnit
    {
        private static readonly string[] DefaultFoldersToScan = { "Assets" };

        [UserSetting] private static readonly GeneratorSetting<bool> ScanAllFoldersSetting = new GeneratorSetting<bool>("resources.scanAllFolders", false);
        [UserSetting] private static readonly GeneratorSetting<string[]> FoldersToScanSetting = new GeneratorSetting<string[]>("resources.foldersToScan", DefaultFoldersToScan);

        /// <summary>
        /// The label to be displayed on the Project Settings menu.
        /// </summary>
        public override GUIContent Label { get; } = new GUIContent("Resources");

        /// <summary>
        /// The special folder name.
        /// </summary>
        protected override string SpecialFolderName { get; } = "Resources";
        /// <summary>
        /// Define what your generated type represent.
        /// </summary>
        protected override string Type { get; } = "Resource";

        /// <summary>
        /// This method is called automatically by the <see cref="Generator"/> if the <see cref="GeneratorExtensionAttribute"/> is present.
        /// </summary>
        public override void DrawSettings(string searchContext)
        {
            DrawContentSettings(searchContext, ScanAllFoldersSetting, FoldersToScanSetting);
        }

        /// <summary>
        /// Implement this to provide your own validation logic for the files to be included in the generated file.
        /// </summary>
        /// <param name="entry">The current file being scanned.</param>
        protected override bool IsValidEntry(string entry)
        {
            if (entry.Contains($"/{SpecialFolderName}/") == false)
            {
                return false;
            }

            if (ScanAllFoldersSetting)
            {
                return true;
            }

            foreach (string folder in FoldersToScanSetting.value)
            {
                if (string.IsNullOrWhiteSpace(folder))
                {
                    continue;
                }

                if (entry.StartsWith($"Assets/{folder}"))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
