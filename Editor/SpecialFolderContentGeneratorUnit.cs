using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.SettingsManagement;
using UnityEditorInternal;
using UnityEngine;

namespace PainfulSmile.StronglyTypedStrings
{
    /// <summary>
    /// Specialized <see cref="GeneratorUnit"/> class to expose strongly-typed paths to the content inside special folders across your project.
    /// </summary>
    public abstract class SpecialFolderContentGeneratorUnit : GeneratorUnit
    {
        private const string FoldersToScanToolTip = "Add here the folders which the generator should search for content.";

        private static readonly GUIContent FoldersToScanLabel = new GUIContent("Folders To Scan", FoldersToScanToolTip);

        private readonly ReorderableList FoldersToScanDrawer = new ReorderableList(null, typeof(string));

        /// <summary>
        /// Customize the order in the Project Settings menu.
        /// </summary>
        protected override int Priority { get; } = 100;
        /// <summary>
        /// The special folder name.
        /// </summary>
        protected abstract string SpecialFolderName { get; }

        /// <summary>
        /// Draw both "Scan All Folders" toggle and "Folders To Scan" list in the editor.
        /// </summary>
        /// <param name="searchContext"></param>
        /// <param name="scanAllFolders"></param>
        /// <param name="foldersToScan"></param>
        protected void DrawContentSettings(string searchContext, GeneratorSetting<bool> scanAllFolders, GeneratorSetting<string[]> foldersToScan)
        {
            scanAllFolders.value = SettingsGUILayout.SettingsToggle("Scan All Folders", scanAllFolders, searchContext);

            if (scanAllFolders || GeneratorUtility.TryMatchSearch(searchContext, FoldersToScanLabel.text) == false)
            {
                return;
            }

            if (AreEqual(foldersToScan.value, FoldersToScanDrawer.list) == false)
            {
                FoldersToScanDrawer.list = foldersToScan.value.ToList();
                FoldersToScanDrawer.onAddCallback = list => list.list.Add(string.Empty);
                FoldersToScanDrawer.onChangedCallback = list => foldersToScan.SetValue(list.list.Cast<string>().ToArray(), true);
                FoldersToScanDrawer.drawHeaderCallback = rect => EditorGUI.LabelField(rect, FoldersToScanLabel);

                FoldersToScanDrawer.drawElementCallback = (rect, index, active, focused) =>
                {
                    using (var changeCheckScope = new EditorGUI.ChangeCheckScope())
                    {
                        string value = EditorGUI.DelayedTextField(rect, GUIContent.none, (string)FoldersToScanDrawer.list[index]);

                        if (changeCheckScope.changed)
                        {
                            FoldersToScanDrawer.list[index] = GeneratorUtility.GetValidFolderPath(value);
                            foldersToScan.value = FoldersToScanDrawer.list.Cast<string>().ToArray();
                        }
                    }
                };
            }

            FoldersToScanDrawer.DoLayoutList();
            SettingsGUILayout.DoResetContextMenuForLastRect(foldersToScan);
        }

        /// <summary>
        /// Implement this to provide your own validation logic for the files to be included in the generated file.
        /// </summary>
        /// <param name="entry">The current file being scanned.</param>
        protected abstract bool IsValidEntry(string entry);

        /// <summary>
        /// Implement your generator logic here.
        /// </summary>
        protected override void OnGenerate()
        {
            var names = new List<string>();
            var paths = new List<string>();
            var pathsWithExtension = new List<string>();
            var duplicates = new List<int>();

            foreach (string assetPath in AssetDatabase.GetAllAssetPaths())
            {
                if (IsValidEntry(assetPath) == false)
                {
                    continue;
                }

                string separator = $"/{SpecialFolderName}/";
                int removeCount = assetPath.IndexOf(separator, StringComparison.Ordinal) + separator.Length;
                string pathWithExtension = assetPath.Remove(0, removeCount);
                string name = Path.GetFileNameWithoutExtension(pathWithExtension);
                string path = pathWithExtension.Replace(Path.GetFileName(pathWithExtension), name);

                if (names.Contains(name))
                {
                    if (pathsWithExtension.Contains(pathWithExtension))
                    {
                        continue;
                    }

                    if (paths.Contains(path))
                    {
                        continue;
                    }

                    if (duplicates.Count == 0)
                    {
                        duplicates.Add(names.IndexOf(name));
                    }

                    duplicates.Add(names.Count);
                }

                names.Add(name);
                paths.Add(path);
                pathsWithExtension.Add(pathWithExtension);
            }

            foreach (int i in duplicates)
            {
                names[i] = paths[i];
            }

            int length = Mathf.Min(names.Count, paths.Count);

            for (int i = 0; i < length; i++)
            {
                Append(GeneratorUtility.GetValidMemberName(names[i]), paths[i]);
            }
        }

        private static bool AreEqual(string[] source, IList comparer)
        {
            if (comparer == null || source.Length != comparer.Count)
            {
                return false;
            }

            for (var i = 0; i < source.Length; i++)
            {
                if (source[i] != (string)comparer[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
