<h1>Strongly-Typed Strings Documentation</h1>

- [Overview](#overview)
- [General](#general)
- [Scenes](#scenes)
- [Resources](#resources)
- [Streaming Assets](#streaming-assets)
- [Editor Default Resources](#editor-default-resources)
- [Gizmos](#gizmos)

<h2>See also</h2>

- [API](API.md)

---

## Overview

---

This is a tool to generate strongly-typed strings to avoid errors when using standard strings to reference stuff in your code. To use it just go to `Tools/Generate Strongly-Typed Strings` and a wait the process to finish. This will generate the following files by default:

| File | Content |
| :-- | :-- |
| `InputType.cs` | Holds the input axes created in Unity's Input menu. You can access the real input name using `InputType.MyInput.GetValue()`. |
| `LayerType.cs` | Holds the layers created in Unity's Tag and Layers menu. You can access the layer index using `(int)LayerType.MyLayer` or the real layer name using `LayerType.MyLayer.GetValue()`. |
| `SceneType.cs` | Holds the scenes added to the Unity's Build Settings menu. You can access the scene index using `(int)SceneType.MyScene` or the scene path by using `SceneType.MyScene.GetValue()`. |
| `SortingLayerType.cs` | Holds the sorting layers created in Unity's Tags and Layers menu. You can access the sorting layer index using `(int)SortingLayerType.MySortingLayer` or the real sorting layer name using `SortingLayerType.MySortingLayer.GetValue()`. |
| `TagType.cs` | Holds the tags created in Unity's Tags and Layers menu. You can access the real tag name using `TagType.MyTag.GetValue()`. |
| `ResourceType.cs` | Holds the assets inside the specials `"Resources"` folders. You can access the actual `"Resources`" relative path by using `ResourceType.MyResource.GetValue()`. |
| `StreamingAssetType.cs` | Holds the assets inside the special `"StreamingAssets"` folders. You can access the actual `"StreamingAssets"` relative path by using `StreamingAssetType.MyAsset.GetValue()`. |
| `EditorDefaultResourceType.cs` | Holds the assets inside the special `"Editor Default Resources"` folders. You can access the actual `"Editor Default Resources"` relative path by using `EditorDefaultResourceType.MyEditorResource.GetValue()`. |
| `GizmoType.cs` | Holds the assets inside the special `"Gizmos"` folders. You can access the actual `"Gizmos"` relative path by using `GizmoType.MyGizmo.GetValue()`. |

**WARNING**: the generated enums are not guarantee to be serialized always on the same order by Unity so you should not use them to input data though the inspector.

You can customize the output of this tool by going to `Edit/Project Settings.../Constants`. There you can also generate only one of the files instead of generating them all at once.

![](main.png)

---

## General

---

Settings that affect all file generators.

| Property | Function |
| :-- | :-- |
| Assembly Definition File | You can choose to generate the files within an existing assembly inside the Assets folder. |
| Assets/Assembly Relative Folder | The Assets-relative or Assembly-relative folder to place the files. |
| Namespace | You can optionally choose a namespace for the generated types. |
| Line Ending | You can choose which line endings to use in the generated files. |

---

## Scenes

---

Scenes-related generator settings.

| Property | Function |
| :-- | :-- |
| Ignore Disabled Scenes | You can choose to ignore disabled scenes when generating the scenes file. |

---

## Resources

---

`"Resources"` folder-related generator settings.

| Property | Function |
| :-- | :-- |
| Scan All Folders | If checked it will search for all `"Resources"` folders inside the project (including inside both Assets and Packages folders). This can cause too many unnecessary stuff to be generated so it is unchecked by default. |
| Folders To Scan | Optionally you can choose specific folders to search for `"Resources"` folders. |

---

## Streaming Assets

---

`"StreamingAssets"` folder-related generator settings.

| Property | Function |
| :-- | :-- |
| Scan All Folders | If checked it will generate strongly-typed strings for the entire content of the `"StreamingAssets"` folder. |
| Folders To Scan | Optionally you can choose specific folders inside the `"StreamingAssets"` folder to generate the strongly-type strings for only some of the content. |

---

## Editor Default Resources

---

`"Editor Default Resources"` folder-related generator settings.

| Property | Function |
| :-- | :-- |
| Scan All Folders | If checked it will generate strongly-typed strings for the entire content of the `"Editor Default Resources"` folder. |
| Folders To Scan | Optionally you can choose specific folders inside the `"Editor Default Resources"` folder to generate the strongly-type strings for only some of the content. |

---

## Gizmos

---

`"Gizmos"` folder-related generator settings.

| Property | Function |
| :-- | :-- |
| Scan All Folders | If checked it will generate strongly-typed strings for the entire content of the `"Gizmos"` folder. |
| Folders To Scan | Optionally you can choose specific folders inside the `"Gizmos"` folder to generate the strongly-type strings for only some of the content. |
