<h1>Strongly-Typed Strings API</h1>

- [Overview](#overview)
- [`GeneratorExtensionAttribute`](#generatorextensionattribute)
- [`GeneratorSetting`](#generatorsetting)
- [`GeneratorUnit`](#generatorunit)
- [`GeneratorUtility`](#generatorutility)
- [`SpecialFolderContentGeneratorUnit`](#specialfoldercontentgeneratorunit)

---

## Overview

---

Here you can check the scripting API for that package. Protected members are marked with an `"*"` on the side.

There are 2 major things this package provides:

- Pre-made strongly-typed strings to replace Unity's strings that references runtime time. You only need to generate the files in the `Project Settings` and start using the generated types in your code.

  Example:

  ```c#
  public static class Player
  {
      private void Start()
      {
          gameObject.layer = (int)LayerType.Player;
          gameObject.tag = TagType.Player.GetValue();
      }
  }
  ```

- An easy way to create new custom strongly-typed strings for your own use case. You only need to:
  - Inherit from [`GeneratorUnit`](#generatorunit)
  - Add the attribute [`GeneratorExtensionAttribute`](#generatorextensionattribute)
  - Implement the required methods
  - Run the generator in the `Project Settings` or in the `Tools` menu

  Example:

  ```c#
  [GeneratorExtension]
  public class MyPersonalGeneratorUnit : GeneratorUnit
  {
      [UserSetting] private static readonly GeneratorSetting<bool> SomeConditionSetting = new GeneratorSetting<bool>("myPersonal.someCondition", true);

      public override GUIContent Label { get; } = new GUIContent("Tests");

      protected override string Type { get; } = "Test";

      public override void DrawSettings(string searchContext)
      {
          SettingsGUILayout.SettingsToggle("Some Condition", SomeConditionSetting, searchContext);
      }

      protected override void OnGenerate()
      {
          if (SomeConditionSetting)
          {
              Append("My Strong Typed String");
          }
      }
  }
  ```

Following the example above, you now will be able to access your custom type the same way you can access the default ones, like `PersonalType.MyStrongTypedString.GetValue()` for example.

---

## `GeneratorExtensionAttribute`

---

Attach this attribute in a class that inherits from  [`GeneratorUnit`](#generatorunit) to expand the Project Settings window.

---

## `GeneratorSetting`

---

Specialized `UserSetting` for any [`GeneratorUnit`](#generatorunit).

---

## `GeneratorUnit`

---

Inherit from that class to create your own file generator.

You can also add the attribute [`GeneratorExtensionAttribute`](#generatorextensionattribute) to expose it on the Project Settings windows.

| Member | Description |
| :--- | :--- |
| Label | The label to be displayed on the Project Settings menu. |
| FileName | The generated file name. |
| Type* | Define what your generated type represent. |
| Priority* | Customize the order in the Project Settings menu. |
| DrawSettings | This method is used to draw it on the Project Settings if the [`GeneratorExtensionAttribute`](#generatorextensionattribute) is present. |
| Generate | Generate the file. |
| OnGenerate* | Implement your generator logic here. |
| Append | Append a value. Allows to also specify a value and/or an id through overloads. |

---

## `GeneratorUtility`

---

General utility class to use on your own [`GeneratorUnit`](#generatorunit).

| Member | Description |
| :--- | :--- |
| GetValidFolderPath | Get a valid folder path based on the received path. |
| GetValidMemberName | Get a valid script member name based on the received name. |
| GetValidNamespaceName | Get a valid script namespace name based on the received name. |
| TryMatchSearch | Check if the search matches the given target. |

---

## `SpecialFolderContentGeneratorUnit`

---

Specialized [`GeneratorUnit`](#generatorunit) class to expose strongly-typed paths to the content inside special folders across your project.

| Member | Description |
| :--- | :--- |
| Priority* | Customize the order in the Project Settings menu. |
| SpecialFolderName* | The special folder name. |
| DrawContentSettings* | Draw both "Scan All Folders" toggle and "Folders To Scan" list in the editor. |
| IsValidEntry* | Implement this to provide your own validation logic for the files to be included in the generated file. |
| OnGenerate* | Implement your generator logic here. |
